package it.uninsubria.socialmusic;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;



import java.util.ArrayList;

public class ResearchActivity extends AppCompatActivity {
    private Spinner spinnerInstrument, spinnerGenre;
    private TextView searchButton;
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.research_activity);

        spinnerGenre = (Spinner)findViewById(R.id.spinnerGenre);
        spinnerInstrument = (Spinner)findViewById(R.id.spinnerInstrument);
        searchButton = (TextView) findViewById(R.id.ricercaTextView);

        ArrayAdapter<CharSequence> adapterInstruments = ArrayAdapter.createFromResource(
                this,
                R.array.instrument_array,
                android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapterGenre = ArrayAdapter.createFromResource(
                this,
                R.array.musical_genre_array,
                android.R.layout.simple_spinner_item);
        spinnerInstrument.setAdapter(adapterInstruments);
        spinnerGenre.setAdapter(adapterGenre);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String instrument = spinnerInstrument.getSelectedItem().toString();
                String genre = spinnerGenre.getSelectedItem().toString();

                ArrayList<String> filters = new ArrayList<>();
                filters.add(instrument);
                filters.add(genre);

                Intent I = new Intent(ResearchActivity.this, ResearchResult.class);
                I.putStringArrayListExtra("filters_list", filters);
                startActivity(I);
            }
        });
    }
}
