package it.uninsubria.socialmusic;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LogInActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private TextView register;
    private TextView login;
    private TextView resetPsw;
    private EditText emailTxt;
    private EditText pswTxt;
    private ProgressBar progressbar;
    private FirebaseUser user;

    @Override
    protected void onStart() {
        super.onStart();
        user = FirebaseAuth.getInstance().getCurrentUser();
        //verificare se utente è già Loggato
        if(user!= null && user.isEmailVerified())
        {
            Intent intent = new Intent(LogInActivity.this,MainHomeActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        register = findViewById(R.id.register);
        login = findViewById(R.id.lgn);
        resetPsw = findViewById(R.id.Psw_Reset);
        emailTxt = findViewById(R.id.usrText);
        pswTxt = findViewById(R.id.pswText);
        progressbar = findViewById(R.id.progressBarLogin);
        mAuth = FirebaseAuth.getInstance();

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(LogInActivity.this, RegisterActivity.class);
                startActivity(I);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressbar.setVisibility(View.VISIBLE);
                String email = emailTxt.getText().toString().trim();
                String psw = pswTxt.getText().toString().trim();
                if (!checkInfo(email,psw)) {
                    mAuth.signInWithEmailAndPassword(email, psw).addOnCompleteListener(LogInActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            progressbar.setVisibility(View.GONE);
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (task.isSuccessful()&& user.isEmailVerified()) {
                                Intent I = new Intent(LogInActivity.this, MainHomeActivity.class);
                                startActivity(I);
                            } else {
                                Toast.makeText(LogInActivity.this, "Authentication Failed!", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });

        resetPsw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent F = new Intent(LogInActivity.this, PasswordReset.class);
                startActivity(F);
            }
        });
    }

    private boolean checkInfo(String mail, String psw){
        return (checkMail(mail) || checkPsw(psw));
    }

    private boolean checkMail(String mail){
        if(Patterns.EMAIL_ADDRESS.matcher(mail).matches())
            return false;
        else{
            progressbar.setVisibility(View.GONE);
            if(mail.isEmpty()){
                emailTxt.setError("Email is required!");
            }
            else{
                emailTxt.setError("Please enter a valid email!");
            }
            emailTxt.requestFocus();
            return true;
        }
    }

    private boolean checkPsw(String psw){
        if (psw.isEmpty()) {
            progressbar.setVisibility(View.GONE);
            pswTxt.setError("Password is required!");
            pswTxt.requestFocus();
            return true;
        }
        else
            return false;
    }
}
