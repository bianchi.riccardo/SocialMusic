package it.uninsubria.socialmusic;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ResearchResult extends AppCompatActivity implements OnMapReadyCallback {
    private RecyclerView profileList;
    private FirebaseFirestore mFireStore;
    private ArrayList<String> filters;
    private FirestoreRecyclerAdapter<User, UserViewHolder> adapter;
    private Dialog mapDialog;
    private GoogleMap mMap;
    private Double latitude, longitude;
    private FirebaseUser mUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.research_result_activity);

        mapDialog = new Dialog(this);
        mapDialog.setContentView(R.layout.custom_popup);
        profileList = (RecyclerView)findViewById(R.id.profileList);
        mFireStore = FirebaseFirestore.getInstance();

        mUser = FirebaseAuth.getInstance().getCurrentUser();

        filters = getIntent().getStringArrayListExtra("filters_list");
        Query query = mFireStore.collection("users").
                whereEqualTo("instrument", filters.get(0)).
                whereEqualTo("favouriteGenre", filters.get(1));



        FirestoreRecyclerOptions<User> options = new FirestoreRecyclerOptions.Builder<User>().
                setQuery(query, User.class).
                build();

        adapter = new FirestoreRecyclerAdapter<User, UserViewHolder>(options) {
            public void onDataChanged() {
                if(getItemCount() == 0)
                    Toast.makeText(getApplicationContext(), "Nessun utente trovato", Toast.LENGTH_SHORT).show();
            }

            @NonNull
            @Override
            public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_single, parent, false);
                return new UserViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull UserViewHolder holder, int position, @NonNull final User model) {
                holder.profileUsername.setText(model.getUsrname());
                holder.profileBornDate.setText(model.getBornDate());
                holder.profileCity.setText(model.getCity());

                Glide.with(ResearchResult.this)
                        .load(model.getProfilePic())
                        .into(holder.profilePic);

                holder.chatButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!mUser.getUid().equals(model.getUserId()))
                            startChat(mUser.getUid(), model.getUserId());
                        else
                            Toast.makeText(getApplicationContext(), "You can't chat with yourself!",Toast.LENGTH_LONG).show();
                    }
                });

                holder.mapButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String citta = model.getCity();
                        showMap(citta);
                    }
                });
            }
        };

        profileList.setLayoutManager(new LinearLayoutManager(this));
        profileList.setAdapter(adapter);
    }

    private static class UserViewHolder extends RecyclerView.ViewHolder {
        private TextView profileUsername;
        private TextView profileBornDate;
        private TextView profileCity;
        private TextView mapButton;
        private TextView chatButton;
        private ImageView profilePic;

        public UserViewHolder(@NonNull View itemView) {
            super(itemView);
            profileUsername = itemView.findViewById(R.id.profileUsername);
            profileBornDate = itemView.findViewById(R.id.profileAge);
            profileCity = itemView.findViewById(R.id.profileCity);
            profilePic = itemView.findViewById(R.id.profilePic);
            mapButton = itemView.findViewById(R.id.mapButton);
            chatButton = itemView.findViewById(R.id.chatButton);
        }
    }

    private void startChat(String senderUId ,String receiverUId) {
        DocumentReference myChatDoc = mFireStore.collection("chats").document();
        DocumentReference receiverChatDoc = mFireStore.collection("chats").document();

        Chat myNewChat = new Chat(myChatDoc.getId(), senderUId, receiverUId);
        myChatDoc.set(myNewChat);
        Chat receiverNewChat = new Chat(receiverChatDoc.getId(), receiverUId, senderUId);
        receiverChatDoc.set(receiverNewChat);

        Intent I = new Intent(ResearchResult.this, MessageActivity.class);
        I.putExtra("receiverId", receiverUId);
        I.putExtra("previousActivity", "ResearchResult");
        startActivity(I);
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    protected void onStop(){
        super.onStop();
        adapter.stopListening();
    }

    public void showMap(String citta){
        mapDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_BACK){
                    mapDialog.dismiss();
                }
                return true;
            }
        });
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses;

        if(Geocoder.isPresent()){
            try {
                addresses = geocoder.getFromLocationName(citta, 1);
                if (addresses.size() > 0) {
                    latitude = addresses.get(0).getLatitude();
                    longitude = addresses.get(0).getLongitude();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapDialog.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.clear();
        LatLng pos = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(pos).title("Posizione"));
        float zoomLevel = 16.0f;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos,zoomLevel));
    }
}
