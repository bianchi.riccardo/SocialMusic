package it.uninsubria.socialmusic;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class PreferenceEditActivity extends AppCompatActivity {
    private Spinner instrumentSpinner, musicalGenre;
    private EditText description;
    private TextView invioDati;
    private ProgressBar progressbar;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preference_edit);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        instrumentSpinner = (Spinner) findViewById(R.id.inst_spn);
        musicalGenre = (Spinner) findViewById(R.id.gen_spn);
        description = (EditText) findViewById(R.id.des);
        invioDati = (TextView) findViewById(R.id.invioTextView);
        progressbar = (ProgressBar) findViewById(R.id.progressBarPreference);

        ArrayAdapter<CharSequence> adapterInstruments = ArrayAdapter.createFromResource(
                this,
                R.array.instrument_array,
                android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapterGenre = ArrayAdapter.createFromResource(
                this,
                R.array.musical_genre_array,
                android.R.layout.simple_spinner_item);

        instrumentSpinner.setAdapter(adapterInstruments);
        musicalGenre.setAdapter(adapterGenre);

        invioDati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressbar.setVisibility(View.VISIBLE);
                final String desc = description.getText().toString().trim();
                final String instrument = instrumentSpinner.getSelectedItem().toString().trim();
                final String genre = musicalGenre.getSelectedItem().toString().trim();
                mUser = mAuth.getCurrentUser();
                String user = mUser.getUid();
                if(!checkDesc(desc)){
                    DocumentReference userReference = db.collection("users").document(user);
                    userReference.update("instrument", instrument);
                    userReference.update("favouriteGenre", genre);
                    userReference.update("description", desc);
                    progressbar.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(),"Registration Completed!",Toast.LENGTH_LONG).show();
                    Intent I = new Intent(PreferenceEditActivity.this, LogInActivity.class);
                    I.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(I);
                }
            }
        });
    }

    private boolean checkDesc(String desc){
        if(desc.length()>120) {
            progressbar.setVisibility(View.GONE);
            description.setError("Description must be shorter than 120 characters!");
            description.requestFocus();
            return true;
        }
        else {
            if(desc.isEmpty()){
                progressbar.setVisibility(View.GONE);
                description.setError(("Description is required!"));
                description.requestFocus();
                return true;
            }
            return false;
        }
    }
}
