package it.uninsubria.socialmusic;

import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

public class UserHomeActivity extends AppCompatActivity{

    private TextView userName;
    private TextView description;
    private TextView city;
    private TextView modifyProfile;
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFireStore;
    private String userId;
    private ImageView prof_pic;
    private RelativeLayout userHome;

    StorageReference storageReference;;

    private ImageView b;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_home_activity);

        userName = findViewById(R.id.userName_txtView);
        description = findViewById(R.id.description_txtView);
        prof_pic = (ImageView) findViewById(R.id.img_user);
        city = findViewById(R.id.city_txtView);
        modifyProfile = findViewById(R.id.modifyProfile);
        b = (ImageView) findViewById(R.id.img_plus);

        mAuth = FirebaseAuth.getInstance();
        mFireStore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();

        StorageReference profileReference = storageReference.child("users/" + mAuth.getCurrentUser().getUid() + "/profile.jpg");
        profileReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).into(prof_pic);
            }
        });

        userId = mAuth.getCurrentUser().getUid();
        final DocumentReference documentReference = mFireStore.collection("users").document(userId);
        documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                userName.setText(documentSnapshot.getString("usrname"));
                description.setText(documentSnapshot.getString("description"));
                city.setText(documentSnapshot.getString("city"));
            }
        });

        b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        modifyProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(UserHomeActivity.this, ModifyProfile.class);

                ArrayList<String> profileInfo = new ArrayList<>();
                profileInfo.add(userName.getText().toString());
                profileInfo.add(description.getText().toString());
                profileInfo.add(city.getText().toString());
                I.putStringArrayListExtra("profileInfo", profileInfo);

                startActivity(I);
            }
        });
    }

    private void selectImage() {
        final CharSequence[] options = {"Scatta Foto", "Scegli dalla Galleria", "Indietro"};
        AlertDialog.Builder builder = new AlertDialog.Builder(UserHomeActivity.this);
        builder.setTitle("Aggiungi Foto!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Scatta Foto")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    Uri photoUri = FileProvider.getUriForFile(UserHomeActivity.this, getApplicationContext().getPackageName() + ".provider", getCameraFile());
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoUri);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(takePictureIntent, 1);
                    }
                } else if (options[item].equals("Scegli dalla Galleria")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Esci")) {
                    dialog.dismiss();
                }
            }

        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Uri photoUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", getCameraFile());
                final StorageReference fileRef = storageReference.child("users/" + mAuth.getCurrentUser().getUid() + "/" + photoUri.getLastPathSegment());
                fileRef.putFile(photoUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Toast.makeText(UserHomeActivity.this, "Immagine Aggiunta!", Toast.LENGTH_SHORT).show();
                        fileRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Picasso.get().load(uri).into(prof_pic);
                                DocumentReference document = mFireStore.collection("users").document(mAuth.getCurrentUser().getUid());
                                document.update("profilePic", uri.toString());
                            }
                        });
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(UserHomeActivity.this, "Impossibile Aggiungere Immagine!", Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                final StorageReference fileRef = storageReference.child("users/" + mAuth.getCurrentUser().getUid() + "/profile.jpg");
                fileRef.putFile(selectedImage).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Toast.makeText(UserHomeActivity.this, "Immagine Aggiunta!", Toast.LENGTH_SHORT).show();
                        /**Per aggiunta in firestore**/
                        fileRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Picasso.get().load(uri).into(prof_pic);
                                DocumentReference document = mFireStore.collection("users").document(mAuth.getCurrentUser().getUid());
                                document.update("profilePic", uri.toString());
                            }
                        });
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(UserHomeActivity.this, "Impossibile Aggiungere Immagine!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    public File getCameraFile() {
        File dir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return new File(dir, "profile.jpg");
    }
}