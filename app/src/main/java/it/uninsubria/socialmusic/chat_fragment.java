package it.uninsubria.socialmusic;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class chat_fragment extends Fragment {

    private List<String> usersList;
    private ArrayList<User> mUsers;
    private RecyclerView recyclerView;
    private ChatAdapter adapter;
    private FirebaseUser fUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_fragment, container, false);

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        fUser = FirebaseAuth.getInstance().getCurrentUser();

        usersList = new ArrayList<>();

        FirebaseFirestore.getInstance().collection("chats").whereEqualTo("member1Id", fUser.getUid()).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e){
                usersList.clear();
                if(queryDocumentSnapshots != null){
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Chat chat = documentSnapshot.toObject(Chat.class);

                        usersList.add(chat.getMember2Id());
                    }

                    createChats();
                }
            }
        });
        return view;
    }

    private void createChats() {
        mUsers = new ArrayList<>();

        FirebaseFirestore.getInstance().collection("users").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                for(QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                    User user = documentSnapshot.toObject(User.class);

                    for(String id : usersList){
                        if(id.equals(user.getUserId())) {
                            if(mUsers.size() != 0) {
                                if(!mUsers.contains(user)){
                                    mUsers.add(user);
                                }
                            }
                            else
                                mUsers.add(user);
                        }
                    }
                }
                adapter = new ChatAdapter(getContext(), mUsers);
                recyclerView.setAdapter(adapter);
            }
        });
    }

    private static class ChatViewHolder extends RecyclerView.ViewHolder{
        private TextView profileUsername;
        private ImageView profilePic;

        public ChatViewHolder(@NonNull View itemView) {
            super(itemView);

            profileUsername = itemView.findViewById(R.id.profileUsername);
            profilePic = itemView.findViewById(R.id.profilePic);
        }
    }

    private class ChatAdapter extends RecyclerView.Adapter<ChatViewHolder>{
        private ArrayList<User> users;
        private Context mContext;

        public ChatAdapter(Context c, ArrayList<User> list){
            this.users = list;
            this.mContext = c;
        }

        @NonNull
        @Override
        public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ChatViewHolder(LayoutInflater.from(mContext).inflate(R.layout.chatlist_single_item,parent,false));
        }

        @Override
        public void onBindViewHolder(@NonNull final ChatViewHolder holder, final int position) {
            holder.profileUsername.setText(users.get(position).getUsrname());

            Glide.with(chat_fragment.this).load(users.get(position).getProfilePic()).into(holder.profilePic);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent I = new Intent(mContext, MessageActivity.class);
                    I.putExtra("receiverId", users.get(position).getUserId());
                    I.putExtra("previousActivity", "ChatActivity");

                    startActivity(I);
                }
            });
        }

        @Override
        public int getItemCount() {
            return users.size();
        }
    }
}