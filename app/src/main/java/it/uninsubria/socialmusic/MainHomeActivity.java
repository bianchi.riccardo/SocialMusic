package it.uninsubria.socialmusic;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

public class MainHomeActivity extends AppCompatActivity {
    private TextView LogOut;
    private RelativeLayout mProfile;
    private RelativeLayout mSearch;
    private RelativeLayout mChat;

    String mUID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_home_activity);

        mProfile = findViewById(R.id.prf_layout);
        mSearch = findViewById(R.id.src_layout);
        mChat = findViewById(R.id.chat_layout);
        LogOut = findViewById(R.id.logout);

        mUID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        LogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(MainHomeActivity.this,LogInActivity.class));
                finish();
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent I = new Intent(MainHomeActivity.this, UserHomeActivity.class);
                startActivity(I);
            }
        });

        mSearch.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v){
                Intent I = new Intent(MainHomeActivity.this, ResearchActivity.class);
                startActivity(I);
            }
        });

        mChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(MainHomeActivity.this, ChatListActivity.class);
                startActivity(I);
            }
        });
    }
}
