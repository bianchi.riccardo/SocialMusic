package it.uninsubria.socialmusic;

import android.content.Intent;
import android.util.Patterns;
import android.widget.*;
import android.view.View;
import android.app.DatePickerDialog;
import android.widget.DatePicker;
import android.os.Bundle;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class RegisterActivity extends AppCompatActivity {
    private EditText mUsrName,mPass,mEmail,mName,mSurname, mCity;
    private String gender = "MASCHIO";
    private TextView bornDate;
    private int mYear, mMonth, mDay;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private String userID = " ";
    private ProgressBar progressbar;

    private FirebaseFirestore db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        mUsrName = (EditText) findViewById(R.id.usrText);
        mPass = (EditText) findViewById(R.id.pswText);
        mEmail = (EditText) findViewById(R.id.usrMail);
        mName = (EditText) findViewById(R.id.usrName);
        mSurname = (EditText) findViewById(R.id.usrSur);
        mCity = (EditText) findViewById(R.id.city);
        bornDate = (TextView) findViewById(R.id.date);
        progressbar = (ProgressBar) findViewById(R.id.progressBarRegister);
        final Calendar mCurrentDate = Calendar.getInstance();
        final Calendar mYesterdayDate = Calendar.getInstance();

        /**Invio dati al DB per registrazione**/
        findViewById(R.id.invioTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressbar.setVisibility(View.VISIBLE);
                final String usr = mUsrName.getText().toString().trim();
                final String password = mPass.getText().toString().trim();
                final String mail = mEmail.getText().toString().trim();
                final String nome = mName.getText().toString().trim();
                final String cognome = mSurname.getText().toString().trim();
                final String city = mCity.getText().toString().trim();
                final String dataDiNascita = bornDate.getText().toString().trim();

                if (!checkInfo(usr, password, mail, nome, cognome, city)) {
                    mAuth.createUserWithEmailAndPassword(mail, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                userID = mAuth.getCurrentUser().getUid();
                                DocumentReference documentReference = db.collection("users").document(userID);
                                User user = new User(mAuth.getCurrentUser().getUid(), usr, nome, cognome, gender, city, null, null, dataDiNascita, null, null);
                                documentReference.set(user);
                                mUser = mAuth.getCurrentUser();
                                mUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        progressbar.setVisibility(View.GONE);
                                        Toast.makeText(getApplicationContext(), "Verification email sent!", Toast.LENGTH_LONG).show();
                                        Intent I = new Intent(RegisterActivity.this, PreferenceEditActivity.class);
                                        startActivity(I);
                                    }
                                });
                            }
                            else{
                                progressbar.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });

        /**Visualizzazione Calendario**/
        mDay = mCurrentDate.get(Calendar.DAY_OF_MONTH);
        mMonth = mCurrentDate.get(Calendar.MONTH);
        mYear = mCurrentDate.get(Calendar.YEAR);
        mYesterdayDate.add(Calendar.DATE,-1);


        bornDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePicker = new DatePickerDialog(RegisterActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        bornDate.setText((dayOfMonth + "/" + (month + 1) + "/" + year));
                    }}, mYear, mMonth, mDay);
                datePicker.getDatePicker().setMaxDate(mYesterdayDate.getTimeInMillis());
                datePicker.show();
            }
        });

        /**Gestione RadioButton Sesso**/
        RadioGroup genderRadioGroup = (RadioGroup) findViewById(R.id.radioGender);
        genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioUnclicked;
                if (checkedId == R.id.radio_man){
                    radioUnclicked = (RadioButton) findViewById((R.id.radio_woman));
                    gender = "MASCHIO";
                }
                else {
                    radioUnclicked = (RadioButton) findViewById((R.id.radio_man));
                    gender = "FEMMINA";
                }
                radioUnclicked.setChecked(false);
            }
        });
    }

    private boolean checkInfo(String usrName,String psw, String email, String nome, String cognome, String citta){
        return (checkUserName(usrName) || checkPassword(psw) || checkMail(email) ||  checkNome(nome) || checkCognome(cognome) || checkCity(citta));
    }

    private boolean checkMail(String email){
        if(Patterns.EMAIL_ADDRESS.matcher(email).matches())
            return false;
        else{
            progressbar.setVisibility(View.GONE);
            if(email.isEmpty()){
                mEmail.setError("Email is required!");
            }
            else {
                mEmail.setError("Email format is wrong!");
            }
            mEmail.requestFocus();
            return true;
        }
    }

    private boolean checkPassword(String psw){
        String pattern = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@!#$%]).{6,})";
        if(psw.matches(pattern))
            return false;
        else{
            progressbar.setVisibility(View.GONE);
            if(psw.isEmpty())
            {
                mPass.setError("Password is required!");
                mPass.requestFocus();
                return true;
            }
            if(psw.length() < 6) {
                mPass.setError("Password must be longer than 5 character!");
            }
            else {
                mPass.setError("Password must contain number, uppercase letter and special character!");
            }
            mPass.requestFocus();
            return true;
        }
    }

    private boolean checkUserName(String usrName){
        String pattern = "^[a-zA-Z0-9_-]{3,15}$";
        if(usrName.matches(pattern))
            return false;
        else{
            progressbar.setVisibility(View.GONE);
            if(usrName.isEmpty()){
                mUsrName.setError("Username is required!");
                mUsrName.requestFocus();
                return true;
            }
            if(usrName.length() <= 3 || usrName.length() >= 15){
                mUsrName.setError("Username must be between 3 and 15 character!");
            }
            else {
                mUsrName.setError("Username can contain only letters, numbers, dashes and underscores!");
            }
            mUsrName.requestFocus();
            return true;
        }
    }

    private boolean checkNome(String nome){
        String pattern = "^[A-Z][a-z]+";
        if(nome.matches(pattern))
            return false;
        else{
            progressbar.setVisibility(View.GONE);
            if(nome.isEmpty()){
                mName.setError("Name is required!");
            }
            else {
                mName.setError("Name must start with an uppercase letter and cannot contain numbers!");
            }
            mName.requestFocus();
            return true;
        }
    }

    private boolean checkCognome(String cognome){
        String pattern = "^[A-Z][a-z]+";
        if(cognome.matches(pattern))
            return false;
        else{
            progressbar.setVisibility(View.GONE);
            if(cognome.isEmpty()){
                mSurname.setError("Surname is required!");
            }
            else {
                mSurname.setError("Surname must start with an uppercase letter and cannot contain numbers!");
            }
            mSurname.requestFocus();
            return true;
        }
    }

    private boolean checkCity(String citta){
        String pattern = "^[a-zA-Z]+(?:[\\s-][a-zA-Z]+)*$";
        if(citta.matches(pattern))
            return false;
        else {
            if (citta.isEmpty()) {
                mCity.setError("City is required!");
            }
            else {
                mCity.setError("City must contain only letters!");
            }
            mCity.requestFocus();
            return true;
        }
    }

}
