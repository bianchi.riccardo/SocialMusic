package it.uninsubria.socialmusic;

import com.google.firebase.firestore.PropertyName;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class Message {
    private String text;
    private String senderId;
    private String receiverId;
    @ServerTimestamp
    private Date timestamp;

    public Message(){}

    public Message(String t, String sender, String receiver){
        this.text = t;
        this.senderId = sender;
        this.receiverId = receiver;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    @PropertyName("senderId")
    public String getSenderId() {
        return senderId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    @PropertyName("receiverId")
    public String getReceiverId() {
        return receiverId;
    }

    @PropertyName("text")
    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @PropertyName("timestamp")
    public Date getTimestamp() {
        return timestamp;
    }
}
