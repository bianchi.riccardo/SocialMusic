package it.uninsubria.socialmusic;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

public class MessageActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView profileUsername;
    private ImageView profilePicture;
    private RecyclerView messageList;
    private EditText messageText;
    private Button sendMessageButton;
    private String receiverId;
    private FirestoreRecyclerAdapter<Message, MessageViewHolder> adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_activity);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String previousActivity = getIntent().getStringExtra("previousActivity");
                if(previousActivity.equals("ChatActivity"))
                    finish();
                else{
                    Intent I = new Intent(MessageActivity.this, MainHomeActivity.class);
                    I.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(I);
                }
            }
        });


        profileUsername = findViewById(R.id.profileUsername);
        profilePicture = findViewById(R.id.profilePic);
        messageList = findViewById(R.id.reyclerview_message_list);
        messageText = findViewById(R.id.edittext_chatbox);
        sendMessageButton = findViewById(R.id.button_chatbox_send);
        receiverId = getIntent().getStringExtra("receiverId");

        final FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseFirestore.getInstance().collection("users").document(receiverId).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                profileUsername.setText(documentSnapshot.getString("usrname"));
                Glide.with(getApplicationContext()).load(documentSnapshot.getString("profilePic")).into(profilePicture);
            }
        });

        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageContent = messageText.getText().toString();
                if(!messageContent.isEmpty())
                    sendMessage(messageContent, mUser.getUid(), receiverId);
                else
                    Toast.makeText(getApplicationContext(), "You cannot send an empty message!", Toast.LENGTH_LONG).show();
            }
        });

        Query query = FirebaseFirestore.getInstance().collection("messages").document(mUser.getUid()).collection(receiverId).orderBy("timestamp");

        FirestoreRecyclerOptions<Message> options = new FirestoreRecyclerOptions.Builder<Message>().setQuery(query, Message.class).build();

        adapter = new FirestoreRecyclerAdapter<Message, MessageViewHolder>(options) {
            public static final int MSG_TYPE_LEFT = 0;
            public static final int MSG_TYPE_RIGHT = 1;

            @Override
            protected void onBindViewHolder(@NonNull MessageViewHolder holder, int position, @NonNull Message model) {
                holder.messageBody.setText(model.getText());
            }

            @NonNull
            @Override
            public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view;
                if(viewType == MSG_TYPE_RIGHT){
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sent_messages, parent, false);
                }
                else{
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_received, parent, false);
                }
                return new MessageViewHolder(view);
            }

            @Override
            public int getItemViewType(int position) {
                Message m = getItem(position);
                if(m.getSenderId().equals(mUser.getUid()))
                    return MSG_TYPE_RIGHT;
                else
                    return MSG_TYPE_LEFT;
            }
        };

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setStackFromEnd(true);

        messageList.setLayoutManager(layoutManager);
        messageList.setAdapter(adapter);
    }

    private void sendMessage(String content, String senderUid, final String receiverUid){
        messageText.setText("");
        final Message newMessage = new Message(content, senderUid, receiverUid);

        DocumentReference myDocument = FirebaseFirestore.getInstance().collection("messages").document(senderUid).collection(receiverUid).document();
        myDocument.set(newMessage);
        DocumentReference receiverDoc = FirebaseFirestore.getInstance().collection("messages").document(receiverUid).collection(senderUid).document();
        receiverDoc.set(newMessage);
    }

    private static class MessageViewHolder extends RecyclerView.ViewHolder{
        private TextView messageBody;

        public MessageViewHolder(@NonNull View itemView) {
            super(itemView);

            messageBody = itemView.findViewById(R.id.text_message_body);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
