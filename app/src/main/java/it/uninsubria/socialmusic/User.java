package it.uninsubria.socialmusic;

import com.google.firebase.firestore.PropertyName;


public class User{
    private String userId;
    private String usrname;
    private String name;
    private String surname;
    private String gender;
    private String city;
    private String instrument;
    private String favouriteGenre;
    private String description;
    private String profilePic;
    private String bornDate;

    public User(){};

    public User (String id, String usr, String name, String surname, String gender, String city, String instrument, String favGen, String birthD, String img, String descr) {
        this.userId = id;
        this.usrname = usr;
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.city = city;
        this.instrument = instrument;
        this.favouriteGenre = favGen;
        this.bornDate = birthD;
        this.profilePic = img;
        this.description = descr;
    }

    @PropertyName("userId")
    public String getUserId(){
        return this.userId;
    }

    public void setUserId(String id){
        this.userId = id;
    }

    @PropertyName("usrname")
    public String getUsrname() {
        return usrname;
    }

    public void setUsrname(String usrname) {
        this.usrname = usrname;
    }

    @PropertyName("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @PropertyName("surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @PropertyName("gender")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @PropertyName("city")
    public String getCity(){
        return city;
    }

    public void setCity(String city){
        this.city = city;
    }

    @PropertyName("instrument")
    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    @PropertyName("favouriteGenre")
    public String getFavouriteGenre() {
        return favouriteGenre;
    }

    public void setFavouriteGenre(String favouriteGenre) {
        this.favouriteGenre = favouriteGenre;
    }

    @PropertyName("bornDate")
    public String getBornDate() {
        return bornDate;
    }

    public void setBornDate(String bornDate) {
        this.bornDate = bornDate;
    }

    @PropertyName("profilePic")
    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    @PropertyName("description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
