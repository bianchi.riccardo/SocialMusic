package it.uninsubria.socialmusic;

import com.google.firebase.firestore.PropertyName;

public class Chat {
    private String chatId;
    private String member1Id;
    private String member2Id;

    public Chat(){};

    public Chat(String Id, String m1Id, String m2Id){
        this.chatId = Id;
        this.member1Id = m1Id;
        this.member2Id = m2Id;
    }

    @PropertyName("chatId")
    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    @PropertyName("member1Id")
    public String getMember1Id() {
        return member1Id;
    }

    public void setMember1Id(String member1Id) {
        this.member1Id = member1Id;
    }

    @PropertyName("member2Id")
    public String getMember2Id() {
        return member2Id;
    }

    public void setMember2Id(String member2Id) {
        this.member2Id = member2Id;
    }
}
