package it.uninsubria.socialmusic;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class PasswordReset extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private  TextView mSend;
    private EditText mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.password_reset);


        mAuth = FirebaseAuth.getInstance();
        mSend =(TextView) findViewById(R.id.rst);
        mUser = (EditText) findViewById(R.id.email);

        mSend.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                String usrEmail =  mUser.getText().toString();

                if(TextUtils.isEmpty(usrEmail))
                {
                    Toast.makeText(PasswordReset.this, "Inserisci il tuo indirizzo email",Toast.LENGTH_LONG).show();
                }
                else
                {
                    mAuth.sendPasswordResetEmail(usrEmail).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful())
                            {
                                Toast.makeText(PasswordReset.this,"Verifica la tua casella di posta",Toast.LENGTH_LONG).show();

                            }
                            else
                            {
                                Toast.makeText(PasswordReset.this,"Errore! Account Inesistente",Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }

            }
        });

    }
}
