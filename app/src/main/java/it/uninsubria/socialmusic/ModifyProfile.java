package it.uninsubria.socialmusic;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;

public class ModifyProfile extends AppCompatActivity {
    private EditText username, description, city;
    private ImageView modifyUsrname, modifyDesc, modifyCity;
    private TextView sendInfo;
    private ArrayList<String> profileInfo;
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFireStore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modify_profile_activity);

        username = findViewById(R.id.usrText);
        description = findViewById(R.id.usrDescription);
        city = findViewById(R.id.usrCity);
        sendInfo = findViewById(R.id.setInfo);
        modifyUsrname = findViewById(R.id.modifyUsrname);
        modifyDesc = findViewById(R.id.modifyDesc);
        modifyCity = findViewById(R.id.modifyCity);

        mAuth = FirebaseAuth.getInstance();
        mFireStore = FirebaseFirestore.getInstance();

        /**Recupero le informazioni del profilo**/
        profileInfo = getIntent().getStringArrayListExtra("profileInfo");

        /**Inserisco il testo nelle EditText**/
        username.setText(profileInfo.get(0));
        description.setText(profileInfo.get(1));
        city.setText(profileInfo.get(2));

        /**Imposto tutte le EditText come non editabili per poi renderle editabili dopo il click dell'icona**/
        username.setEnabled(false);
        description.setEnabled(false);
        city.setEnabled(false);

        modifyUsrname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username.setEnabled(true);
                username.setText("");
                username.requestFocus();
            }
        });

        modifyDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setEnabled(true);
                description.setText("");
                description.requestFocus();
            }
        });

        modifyCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                city.setEnabled(true);
                city.setText("");
                city.requestFocus();
            }
        });

        sendInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**Leggo i dati modificati e aggiorno il database**/
                String newUsername = "", newDescription = "", newCity = "";
                if (username.isEnabled()) {
                    newUsername = username.getText().toString();
                }
                if (description.isEnabled()) {
                    newDescription = description.getText().toString();
                }
                if (city.isEnabled()) {
                    newCity = city.getText().toString();
                }

                sendNewInfo(newUsername, newDescription, newCity);
            }
        });
    }

    private void sendNewInfo(String newUsername, String newDescription, String newCity) {
        DocumentReference documentReference = mFireStore.collection("users").document(mAuth.getCurrentUser().getUid());
        if (!checkInfo(newUsername, newDescription, newCity)) {
            if(!newUsername.equals(""))
                documentReference.update("usrname", newUsername);
            if(!newDescription.equals(""))
                documentReference.update("description", newDescription);
            if(!newCity.equals(""))
                documentReference.update("city", newCity);

            /**Torno alla UserHomeActivity**/
            Intent I = new Intent(ModifyProfile.this, UserHomeActivity.class);
            I.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(I);
        }
    }

    private boolean checkInfo(String newUsername, String newDescription, String newCity) {
        return (checkUsername(newUsername) || checkDescription(newDescription) || checkCity(newCity));
    }

    private boolean checkUsername(String newUsrName) {
        String pattern = "^[a-zA-Z0-9_-]{3,15}$";
        if (newUsrName.matches(pattern) || newUsrName.isEmpty())
            return false;
        else {
            if ((newUsrName.length() <= 3) || newUsrName.length() >= 15) {
                username.setError("Username must be between 3 and 15 character!");
            } else {
                username.setError("Username can contain only letters, numbers, dashes and underscores!");
            }
            username.requestFocus();
            return true;
        }
    }

    private boolean checkDescription(String newDesc){
        if(newDesc.length()>120) {
            description.setError("Description must be shorter than 120 characters!");
            description.requestFocus();
            return true;
        }
        return false;
    }

    private boolean checkCity(String newCity){
        String pattern = "^[a-zA-Z]+(?:[\\s-][a-zA-Z]+)*$";
        if(newCity.matches(pattern) || newCity.isEmpty())
            return false;
        else{
            city.setError("City must contain only letters!");
            city.requestFocus();
            return true;
        }
    }
}
